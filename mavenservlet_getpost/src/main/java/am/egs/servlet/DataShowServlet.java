package am.egs.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by User on 4/20/2019.
 */
public class DataShowServlet extends HttpServlet {
    /**
     * Get Method.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
            out.println("<html><body><h1>GET Method</h1>");
            out.println("<h1>name="+request.getParameter("name")+"</h1>");
            out.println("<h1>firstname="+request.getParameter("firstname")+"</h1>");
            out.println("</body></html>");
    }

    /**
     * Post Method.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body><h1>Post Method</h1>");
        out.println("<h1>city="+request.getParameter("city")+"</h1>");
        out.println("<h1>address="+request.getParameter("address")+"</h1>");
        out.println("</body></html>");
    }
}
