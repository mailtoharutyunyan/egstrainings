package am.egs.lessons.jpademo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import am.egs.lessons.jpademo.repository.ContactRepository;
import am.egs.lessons.jpademo.configuration.JpaDemoConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import am.egs.lessons.jpademo.entity.Contact;
import am.egs.lessons.jpademo.entity.Contact_Note;
import am.egs.lessons.jpademo.type.PhoneType;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {JpaDemoConfig.class})
public class ContactRepositoryTest {

	@Autowired
	private ContactRepository contactRepo;

	@Test
	public void saveTest() {
		Contact contact = new Contact("Mary", "Zheng", "test@test.com", PhoneType.HOME, "6365272943");
		Contact_Note note = new Contact_Note();
		note.setMessage("She is a java geek");
		contact.addNote(note);	
		
		contactRepo.save(contact);
		
		List<Contact> contacts = contactRepo.findAll();

		Assert.assertTrue(contacts.size() > 0);
	}

	@Test
	public void deleteAndFindTest() {
		Contact contact = new Contact("Mary2", "Zheng2", "test2@test.com", PhoneType.MOBILE, "63652729432");
		Contact_Note note = new Contact_Note();
		note.setMessage("She is a java geek");
		contact.addNote(note);
		
		contactRepo.save(contact);
		List<Contact> contactMary2 = contactRepo.findByLastNameAndPhoneType(PhoneType.HOME, "Zheng");
		contactRepo.delete(contactMary2.get(0));

		contactMary2 = contactRepo.findByLastNameAndPhoneType(PhoneType.HOME, "Zheng");
		assertEquals(0, contactMary2.size());

	}


}
