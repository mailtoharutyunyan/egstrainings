package am.egs.lessons.jpademo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "am.egs.lessons.jpademo")
public class JpaDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(JpaDemoApplication.class, args);
	}
}
