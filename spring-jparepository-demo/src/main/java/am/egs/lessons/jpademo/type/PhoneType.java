package am.egs.lessons.jpademo.type;

public enum PhoneType {
	OFFICE, HOME, MOBILE, OTHER;
}
