package am.egs.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by User on 4/20/2019.
 */
public class FilterServlet implements Filter {


    public void init(FilterConfig fc) throws ServletException {}

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        PrintWriter out = servletResponse.getWriter();
        String pass = servletRequest.getParameter("pass");
        if(pass.equals("1234"))
        {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else
        {
            out.println("You have enter a wrong password");
            RequestDispatcher rs = servletRequest.getRequestDispatcher("index.html");
            rs.include(servletRequest, servletResponse);
        }
    }
    public void destroy() { }
}