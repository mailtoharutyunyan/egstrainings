DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `countryName` varchar(45) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*Table structure for table `user` */

CREATE TABLE `user` 
( 
             `username`  VARCHAR(45) NOT NULL, 
             `password`  VARCHAR(45) NULL, 
             `firstname` VARCHAR(45) NOT NULL, 
             `lastname`  VARCHAR(45) NULL, 
             `email`     VARCHAR(45) NULL, 
             `address`   VARCHAR(45) NULL, 
             `phone`     INT NULL, 
             PRIMARY KEY (`username`) 
)