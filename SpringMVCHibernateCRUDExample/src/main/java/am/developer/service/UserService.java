package am.developer.service;

import am.developer.dao.UserDAO;
import am.developer.model.User;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional
public class UserService {

    @Autowired
    private UserDAO userDao;

    public void register(User user) {
        userDao.register(user);
    }
    
    public List<User> validateUser(User login) {
        return userDao.validateUser(login);
    }
}
