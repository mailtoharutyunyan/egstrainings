package am.egs.spring.test;

import am.egs.spring.config.JpaDemoConfig;
import am.egs.spring.entitiy.Answer;
import am.egs.spring.entitiy.Question;
import am.egs.spring.entitiy.QuestionPack;
import am.egs.spring.repository.QuestionPackRepository;
import am.egs.spring.type.AnswerType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by haykh on 6/5/2019.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaDemoConfig.class})
public class QuestionPackRepositoryTest {

  @Autowired
  private QuestionPackRepository questionPackRepository;
/*
  @Test
  public void saveTest() {
    QuestionPack questionPack = new QuestionPack();
    questionPack.setName("Question 1");
      //question
      List<Question> questionList = new ArrayList<Question>();
      Question question1 = new Question();
      question1.setText("Please describe why should use JPA");
      question1.setQuestionPack(questionPack);
      questionList.add(question1);

      Question question2 = new Question();
      question2.setText("Please describe different beatween JPA and Hibernate");
      question2.setQuestionPack(questionPack);
      questionList.add(question2);
        ///////////////
        List<Answer> answerList = new ArrayList<Answer>();
        Answer answer1ForQuestion1 = new Answer();
        answer1ForQuestion1.setAnswerType(AnswerType.RADIOBUTTON);
        answer1ForQuestion1.setIsCorrectAnswer(1);
        answer1ForQuestion1.setLabelId("lb_yes");
        answer1ForQuestion1.setQuestion(question1);
        answerList.add(answer1ForQuestion1);

        Answer answer2ForQuestion1 = new Answer();
        answer2ForQuestion1.setAnswerType(AnswerType.RADIOBUTTON);
        answer2ForQuestion1.setIsCorrectAnswer(0);
        answer2ForQuestion1.setLabelId("lb_no");
        answer2ForQuestion1.setQuestion(question1);
        answerList.add(answer2ForQuestion1);

        Answer answer3ForQuestion1 = new Answer();
        answer3ForQuestion1.setAnswerType(AnswerType.RADIOBUTTON);
        answer3ForQuestion1.setIsCorrectAnswer(0);
        answer3ForQuestion1.setLabelId("lb_none");
        answer3ForQuestion1.setQuestion(question1);
        answerList.add(answer3ForQuestion1);

        question1.setAnswerList(answerList);
        ////////////////////////////////

    questionPack.setQuestionList(questionList);
    questionPack.setId(1000L);
    questionPackRepository.save(questionPack);

    List<QuestionPack> questionPacks = questionPackRepository.findAll();

    Assert.assertTrue(questionPacks.size() > 0);
  }
*/

  @Test
  public void deleteAndDisassociatesTest() {
    Optional<QuestionPack> byId = questionPackRepository.findById(1000L);
    if(byId.isPresent()){
      QuestionPack qp = byId.get();
      //Disassociates
      List<Question> questionList = new ArrayList();
      for(Question q : qp.getQuestionList()){
        q.setQuestionPack(null);
        questionList.add(q);
      }
      //
      questionPackRepository.saveAndFlush(qp);
      //questionPackRepository.delete(qp);
    }
  }
}
