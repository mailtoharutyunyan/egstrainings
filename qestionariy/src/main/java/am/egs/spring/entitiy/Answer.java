package am.egs.spring.entitiy;

/**
 * Created by haykh on 6/5/2019.
 */
import am.egs.spring.type.AnswerType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "answer")
public class Answer {
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private Long id;

  @Column(name = "LABEL_ID")
  private String labelId;

  @Column(name = "IS_CORRECT_ANSWER")
  private Integer isCorrectAnswer;

  @Column(name = "ANSWER_TYPE")
  @Enumerated(EnumType.STRING)
  private AnswerType answerType;

  @ManyToOne
  @JoinColumn(name = "QUESTION_ID")
  private Question question;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLabelId() {
    return labelId;
  }

  public void setLabelId(String labelId) {
    this.labelId = labelId;
  }

  public Integer getIsCorrectAnswer() {
    return isCorrectAnswer;
  }

  public void setIsCorrectAnswer(Integer isCorrectAnswer) {
    this.isCorrectAnswer = isCorrectAnswer;
  }

  public AnswerType getAnswerType() {
    return answerType;
  }

  public void setAnswerType(AnswerType answerType) {
    this.answerType = answerType;
  }

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }
}
