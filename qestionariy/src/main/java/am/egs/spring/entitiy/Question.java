package am.egs.spring.entitiy;

/**
 * Created by haykh on 6/5/2019.
 */
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "question")
public class Question {
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private Long id;

  @Column(name = "TEXT")
  private String text;

  @ManyToOne
  @JoinColumn(name = "PACK_ID")
  private QuestionPack questionPack;

  @OneToMany(mappedBy="question", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Answer.class)
  private List<Answer> answerList;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public QuestionPack getQuestionPack() {
    return questionPack;
  }

  public void setQuestionPack(QuestionPack questionPack) {
    this.questionPack = questionPack;
  }

  public List<Answer> getAnswerList() {
    return answerList;
  }

  public void setAnswerList(List<Answer> answerList) {
    this.answerList = answerList;
  }
}
