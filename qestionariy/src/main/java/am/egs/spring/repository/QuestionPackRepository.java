package am.egs.spring.repository;

import am.egs.spring.entitiy.QuestionPack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by haykh on 6/5/2019.
 */
@Repository
@Transactional
public interface QuestionPackRepository extends JpaRepository<QuestionPack, Long> {

}
