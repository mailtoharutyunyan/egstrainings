package am.egs.spring.type;

/**
 * Created by haykh on 6/5/2019.
 */
public enum AnswerType {
  CHECKBOX, RADIOBUTTON, SELECTBOX
}
