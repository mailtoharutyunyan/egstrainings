package am.egs.spring.config.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Initializer class to enable the Spring Security configuration.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}