package am.egs.spring.config;

import am.egs.spring.beans.UserAccountBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@Configuration
@ComponentScan({ "am.egs.spring.web.controller" })
@Import({ SecurityConfig.class , JpaDemoConfig.class })

public class AppConfig extends WebMvcConfigurerAdapter {
	public static final String ENCODING_UTF = "UTF-8";

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean(name = "messageSource")
	public MessageSource getMessageSource() {
		ReloadableResourceBundleMessageSource ret = new ReloadableResourceBundleMessageSource();

		ret.setBasename("classpath:messages");

		ret.setCacheSeconds(1);

		ret.setUseCodeAsDefaultMessage(true);

		ret.setDefaultEncoding(ENCODING_UTF);

		return ret;
	}

	/* Register the UserAccountbean. */
	@Bean(name = "userAccountBean")
	public UserAccountBean getUserAccountBean() {
		UserAccountBean ret = new UserAccountBean();
		return ret;
	}

}