package am.egs.spring.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@ComponentScan(
    basePackages = { "am.egs.spring.*"},
    excludeFilters = @Filter(type = FilterType.ANNOTATION,
        value = Configuration.class))
@Configuration
@EnableJpaRepositories("am.egs.spring.repository")
@PropertySource("classpath:db.properties")
public class JpaDemoConfig {

  @Value( "${db.url}" )
  private String dbUrl;

  @Value( "${db.username}" )
  private String dbUsername;

  @Value( "${db.password}" )
  private String dbPassword;

  @Value( "${db.driverClassName}" )
  private String dbDriver;

  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    EclipseLinkJpaVendorAdapter bean = new EclipseLinkJpaVendorAdapter();
    bean.setDatabase(Database.MYSQL);
    bean.setGenerateDdl(true);
    bean.setShowSql(false);
    return bean;
  }

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(dbDriver);
    dataSource.setUrl(dbUrl);
    dataSource.setUsername(dbUsername);
    dataSource.setPassword(dbPassword);
    return dataSource;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
    bean.setDataSource(dataSource);
    bean.setJpaVendorAdapter(jpaVendorAdapter());
    bean.setPackagesToScan(new String[] { "am.egs.spring.entitiy" } );
    Properties properties = new Properties();
    properties.setProperty("eclipselink.weaving", "false");
    bean.setJpaProperties(properties);
    bean.afterPropertiesSet();

    return bean;
  }

  @Bean
  public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
    return new JpaTransactionManager(emf);
  }
}
