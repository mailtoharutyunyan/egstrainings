package am.egs.logback;

/**
 * Created by haykh on 5/7/2019.
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogBackExample {

  private static final Logger logger =
      LoggerFactory.getLogger(LogBackExample.class);

  public static void main(String[] args) {
    logger.debug("Hello from Logback");
  }

}
