package am.egs.hibernate.service.test;

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.bean.EmployeeBean;
import am.egs.hibernate.config.SpringRootConfig;
import am.egs.hibernate.service.EmployeeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by haykh on 5/23/2019.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringRootConfig.class})
public class EmployeeServiceTest {

  @Autowired
  private EmployeeService employeeService;

  @Test
  public void addUserTest() {
    EmployeeBean employeeBean = new EmployeeBean();
    employeeBean.setName("Employee1");

    DepartmentBean departmentBean = new DepartmentBean();
    departmentBean.setName("Department1");
    employeeBean.setDepartment(departmentBean);

    employeeService.addEmployee(employeeBean);
  }

  @Test
  public void getAllEmployeesTest() {
    Assert.assertNotNull(employeeService.getAllEmployees());
  }

}
