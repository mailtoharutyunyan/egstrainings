package am.egs.hibernate.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haykh on 5/23/2019.
 */
public class DepartmentBean {
  private long id;
  private String name;
  private List<EmployeeBean> employees = new ArrayList<>();

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<EmployeeBean> getEmployees() {
    return employees;
  }

  public void setEmployees(List<EmployeeBean> employees) {
    this.employees = employees;
  }
}
