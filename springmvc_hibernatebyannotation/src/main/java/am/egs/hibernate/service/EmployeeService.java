package am.egs.hibernate.service;

import am.egs.hibernate.bean.EmployeeBean;
import java.util.List;

/**
 * Created by haykh on 5/23/2019.
 */
public interface EmployeeService {

  void addEmployee(EmployeeBean employee);

  List<EmployeeBean> getAllEmployees();

  void deleteEmployee(Integer employeeId);

  EmployeeBean getEmployee(int employeeid);

  EmployeeBean updateEmployee(EmployeeBean employee);

}
