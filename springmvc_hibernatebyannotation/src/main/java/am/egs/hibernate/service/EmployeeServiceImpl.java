package am.egs.hibernate.service;

/**
 * Created by haykh on 5/23/2019.
 */
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import am.egs.hibernate.dao.EmployeeDAO;
import am.egs.hibernate.entity.Employee;
import am.egs.hibernate.bean.EmployeeBean;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  private EmployeeDAO employeeDAO;

  @Override
  @Transactional
  public void addEmployee(EmployeeBean employeeBean) {
    Employee employee = new Employee();
    BeanUtils.copyProperties(employeeBean, employee);
    employeeDAO.addEmployee(employee);
  }

  @Override
  @Transactional
  public List<EmployeeBean> getAllEmployees() {
    List<EmployeeBean> employeeBeanList = new ArrayList<>();
    List<Employee> allEmployees = employeeDAO.getAllEmployees();
    BeanUtils.copyProperties(employeeBeanList, allEmployees);
    return employeeBeanList;
  }

  @Override
  @Transactional
  public void deleteEmployee(Integer employeeId) {
    employeeDAO.deleteEmployee(employeeId);
  }

  public EmployeeBean getEmployee(int empid) {
    return null;//employeeDAO.getEmployee(empid);
  }

  public EmployeeBean updateEmployee(EmployeeBean employee) {
    // TODO Auto-generated method stub
    return null;//employeeDAO.updateEmployee(employee);
  }

  public void setEmployeeDAO(EmployeeDAO employeeDAO) {
    this.employeeDAO = employeeDAO;
  }

}

