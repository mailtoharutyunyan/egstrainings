package am.egs.hibernate.service;

/**
 * Created by haykh on 5/23/2019.
 */

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.bean.EmployeeBean;
import am.egs.hibernate.dao.DepartmentDAO;
import am.egs.hibernate.dao.EmployeeDAO;
import am.egs.hibernate.entity.Department;
import am.egs.hibernate.entity.Employee;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

  @Autowired
  private DepartmentDAO departmentDAO;

  @Override
  @Transactional
  public void addDepartment(DepartmentBean departmentBean) {
    Department department = new Department();
    //convert employess
    List<Employee> employees = new ArrayList<>();
    for(EmployeeBean employeeBean : departmentBean.getEmployees()){
      Employee employee = new Employee();
      BeanUtils.copyProperties(employeeBean, employee);
      employees.add(employee);
    }

    BeanUtils.copyProperties(departmentBean, department);

    department.setEmployees(employees);
    departmentDAO.addDepartment(department);
  }

  @Override
  @Transactional
  public List<DepartmentBean> getAllDepartment() {
    List<DepartmentBean> departmentBeanList = new ArrayList<>();
    List<Department> allDepartment = departmentDAO.getAllDepartment();
    BeanUtils.copyProperties(departmentBeanList, allDepartment);
    return departmentBeanList;
  }

  @Override
  @Transactional
  public void deleteDepartment(Integer departmentId) {
    departmentDAO.deleteDepartment(departmentId);
  }

  public DepartmentBean getDepartment(int depid) {
    return null;//departmentDAO.getDepartment(depid);
  }

  public DepartmentBean updateDepartment(DepartmentBean departmentBean) {
    // TODO Auto-generated method stub
    return null;//departmentDAO.updateDepartment(new Department());
  }

}

