package am.egs.hibernate.service;

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.entity.Department;
import java.util.List;

/**
 * Created by haykh on 5/23/2019.
 */
public interface DepartmentService {

  void addDepartment(DepartmentBean departmentBean);

  List<DepartmentBean> getAllDepartment();

  void deleteDepartment(Integer departmentId);

  DepartmentBean getDepartment(int departmentid);

  DepartmentBean updateDepartment(DepartmentBean department);

}
