package am.egs.hibernate.web;

import am.egs.hibernate.bean.EmployeeBean;
import java.util.List;
import java.util.Map;

import am.egs.hibernate.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {

	private final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView  index(Map<String, Object> model) {
		logger.info("index() is executed!");
		//return "index";

		List<EmployeeBean> employees = employeeService.getAllEmployees();
		logger.debug("Getting phones of the selected user from db");
		ModelAndView modelAndView = new ModelAndView("phones");
		modelAndView.setViewName("index");
		modelAndView.addObject("employees", employees);
		return modelAndView;

	}

}