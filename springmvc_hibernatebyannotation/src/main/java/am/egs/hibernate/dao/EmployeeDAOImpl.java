package am.egs.hibernate.dao;

/**
 * Created by haykh on 5/23/2019.
 */

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import am.egs.hibernate.entity.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

  //@Autowired
  //private SessionFactory sessionFactory;

  @Autowired
  private SessionFactory myHibernateSessionFactory;


  public void addEmployee(Employee employee) {

    Session session = null;
    Transaction transaction = null;
    try {
      session = myHibernateSessionFactory.openSession();
      transaction = session.getTransaction();
      transaction.begin();

      //myHibernateSessionFactory.openSession().saveOrUpdate(employee);
      session.persist(employee);

      transaction.commit();
    } catch (Exception e) {
      if (transaction != null) {
        transaction.rollback();
      }
      e.printStackTrace();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }

  @SuppressWarnings("unchecked")
  public List<Employee> getAllEmployees() {
    Query query = myHibernateSessionFactory.openSession().createQuery("from Employee");

    List list = query.list();
    myHibernateSessionFactory.close();


    if ((list == null) || (list.size() == 0)) {
      return list;
    }
    return null;
  }

  @Override
  public void deleteEmployee(Integer employeeId) {
    Employee employee = (Employee) myHibernateSessionFactory.getCurrentSession().load(
        Employee.class, employeeId);
    if (null != employee) {
      myHibernateSessionFactory.openSession().delete(employee);
    }

  }

  public Employee getEmployee(int empid) {
    return (Employee) myHibernateSessionFactory.openSession().get(
        Employee.class, empid);
  }

  @Override
  public Employee updateEmployee(Employee employee) {
    myHibernateSessionFactory.getCurrentSession().update(employee);
    return employee;
  }

}

