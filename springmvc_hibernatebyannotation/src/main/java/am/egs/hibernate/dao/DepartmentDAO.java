package am.egs.hibernate.dao;

import am.egs.hibernate.bean.DepartmentBean;
import am.egs.hibernate.entity.Department;
import java.util.List;

/**
 * Created by haykh on 5/23/2019.
 */
public interface DepartmentDAO {

  void addDepartment(Department department);

  List<Department> getAllDepartment();

  void deleteDepartment(Integer departmentId);

  Department getDepartment(int departmentid);

  Department updateDepartment(Department department);

}
