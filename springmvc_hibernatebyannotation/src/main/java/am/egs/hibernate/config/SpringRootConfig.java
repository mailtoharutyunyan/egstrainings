package am.egs.hibernate.config;

import am.egs.hibernate.entity.Department;
import am.egs.hibernate.entity.Employee;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({"am.egs.hibernate.service", "am.egs.hibernate.dao"})
@PropertySource("classpath:db.properties")
public class SpringRootConfig {
  @Value( "${db.url}" )
  private String dbUrl;

  @Value( "${db.username}" )
  private String dbUsername;

  @Value( "${db.password}" )
  private String dbPassword;

  @Value( "${db.driverClassName}" )
  private String dbDriver;

  private static StandardServiceRegistry registry;

  @Bean
  public SessionFactory myHibernateSessionFactory() {
    SessionFactory sessionFactory = null;
      try {
        StandardServiceRegistryBuilder registryBuilder =
            new StandardServiceRegistryBuilder();

        Map<String, String> settings = new HashMap<>();
        settings.put("hibernate.connection.driver_class", dbDriver);
        settings.put("hibernate.connection.url", dbUrl);
        settings.put("hibernate.connection.username", dbUsername);
        settings.put("hibernate.connection.password", dbPassword);
        settings.put("hibernate.show_sql", "true");
        settings.put("hibernate.hbm2ddl.auto", "update");

        registryBuilder.applySettings(settings);

        registry = registryBuilder.build();

        MetadataSources sources = new MetadataSources(registry)
            .addAnnotatedClass(Employee.class)
            .addAnnotatedClass(Department.class);

        Metadata metadata = sources.getMetadataBuilder().build();

        sessionFactory = metadata.getSessionFactoryBuilder().build();
      } catch (Exception e) {
        System.out.println("SessionFactory creation failed");
        if (registry != null) {
          StandardServiceRegistryBuilder.destroy(registry);
        }
      }
    return sessionFactory;
  }
}