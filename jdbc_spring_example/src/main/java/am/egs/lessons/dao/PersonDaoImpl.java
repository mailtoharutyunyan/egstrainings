package am.egs.lessons.dao;

/**
 * Created by haykh on 5/17/2019.
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import am.egs.lessons.dto.PersonDTO;

@Repository
@Qualifier("personDao")
public class PersonDaoImpl implements PersonDao {

  @Autowired
  JdbcTemplate jdbcTemplate;

  public void addPerson(PersonDTO person) {
    jdbcTemplate.update("INSERT INTO trn_person (person_id, first_name, Last_name, age) VALUES (?, ?, ?, ?)",
        person.getPersonId(), person.getFirstName(), person.getLastName(), person.getAge());
    System.out.println("Person Added!!");
  }

  public void editPerson(PersonDTO person, int personId) {
    jdbcTemplate.update("UPDATE trn_person SET first_name = ? , last_name = ? , age = ? WHERE person_id = ? ",
        person.getFirstName(), person.getLastName(), person.getAge(), personId);
    System.out.println("Person Updated!!");
  }

  public void deletePerson(int personId) {
    jdbcTemplate.update("DELETE from trn_person WHERE person_id = ? ", personId);
    System.out.println("Person Deleted!!");
  }

  public PersonDTO find(int personId) {
    PersonDTO person = (PersonDTO) jdbcTemplate.queryForObject("SELECT * FROM trn_person where person_id = ? ",
        new Object[] { personId }, new BeanPropertyRowMapper(PersonDTO.class));

    return person;
  }

  public List <PersonDTO> findAll() {
    List <PersonDTO> persons = jdbcTemplate.query("SELECT * FROM trn_person", new BeanPropertyRowMapper(PersonDTO.class));
    return persons;
  }
}
