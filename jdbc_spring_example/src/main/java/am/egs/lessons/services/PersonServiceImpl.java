package am.egs.lessons.services;

/**
 * Created by haykh on 5/17/2019.
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import am.egs.lessons.dto.PersonDTO;
import am.egs.lessons.dao.PersonDao;

@Service("personService")
public class PersonServiceImpl implements PersonService {

  @Autowired
  PersonDao personDao;

  public void addPerson(PersonDTO person) {
    personDao.addPerson(person);

  }

  public void editPerson(PersonDTO person, int personId) {
    personDao.editPerson(person, personId);
  }

  public void deletePerson(int personId) {
    personDao.deletePerson(personId);
  }

  public PersonDTO find(int personId) {
    return personDao.find(personId);
  }

  public List <PersonDTO> findAll() {
    return personDao.findAll();
  }
}
