package am.egs.lessons.services;

/**
 * Created by haykh on 5/17/2019.
 */
import java.util.List;

import am.egs.lessons.dto.PersonDTO;

public interface PersonService {

  public void addPerson(PersonDTO person);

  public void editPerson(PersonDTO person, int personId);

  public void deletePerson(int personId);

  public PersonDTO find(int personId);

  public List <PersonDTO> findAll();
}
