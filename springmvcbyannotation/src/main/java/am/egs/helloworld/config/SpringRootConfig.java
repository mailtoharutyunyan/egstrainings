package am.egs.helloworld.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "am.egs.helloworld.service" })
public class SpringRootConfig {
}