package am.egs.entrypoint;

import am.egs.service.BusinessServices;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by haykh on 4/29/2019.
 */
public class RunApp {
  public static void main( String[] args )
  {
    ApplicationContext context =
        new ClassPathXmlApplicationContext(new String[] {"bean.xml"});

    BusinessServices bs = (BusinessServices)context.getBean("businessServices");
    bs.doSomething();

  }
}
