package egs.bean;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by haykh on 4/23/2019.
 */

public class Person implements Serializable {

  @Setter @Getter
  private String name;
  @Setter @Getter
  private String email;
}
