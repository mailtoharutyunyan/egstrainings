package am.egs.lessons.db;

/**
 * Created by haykh on 5/16/2019.
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {
  private static final Logger logger =
          LoggerFactory.getLogger(ConnectionManager.class);

  private static Connection connection = null;
  private static String DB_URL;
  private static String DB_USER;
  private static String DB_PASS;
  private static String DB_DRIVER_MANAGER;

  private static ConnectionManager instance;

  public static ConnectionManager getInstance() {
    if (instance == null) {
      synchronized (ConnectionManager.class) {
        if (instance == null) {
          instance = new ConnectionManager();
        }
      }
    }
    return instance;
  }

  public ConnectionManager() {
    loadProperties();
  }

  private void loadProperties () {
    Properties properties = new Properties();
    try (InputStream inStream = ConnectionManager.class.getClassLoader().getResourceAsStream("db.properties")) {
      properties.load(inStream);
      DB_URL = properties.getProperty("db.url");
      DB_USER = properties.getProperty("db.username");
      DB_PASS = properties.getProperty("db.password");
      DB_DRIVER_MANAGER = properties.getProperty("db.driverClassName");
    } catch (IOException ioe) {
      logger.error("loadProperties error"+ioe);
      throw new IllegalStateException(ioe);

    }
  }

  public Connection getConnection() {
    try {
      if (connection == null || connection.isClosed()) {
        //try {
          Class.forName(DB_DRIVER_MANAGER);
          connection = java.sql.DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        /*} catch (ClassNotFoundException | SQLException e) {
          e.printStackTrace();
        }*/
      }
    } catch (Exception e) {
      logger.error("getConnection error" + e);
    }
    return connection;
  }

  public static void disconnect() {
    try {
      if (connection != null && !connection.isClosed()) {
        connection.close();
      }
    } catch (SQLException ex){
      logger.error("disconnect" + ex);
    }
  }

  public void setProperties () {
    Properties properties = new Properties();
    try (InputStream inStream = ConnectionManager.class.getClassLoader().getResourceAsStream("db.properties")) {
      properties.load(inStream);
      DB_URL = properties.getProperty("h2.db.url");
      DB_USER = properties.getProperty("h2.db.username");
      DB_PASS = properties.getProperty("h2.db.password");
      DB_DRIVER_MANAGER = properties.getProperty("h2.db.driverClassName");
      connection = null;
    } catch (IOException ioe) {
      logger.error("setProperties" + ioe);
      throw new IllegalStateException(ioe);
    }
  }



}

