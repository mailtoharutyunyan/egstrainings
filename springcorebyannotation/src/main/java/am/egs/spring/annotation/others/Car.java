package am.egs.spring.annotation.others;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by haykh on 4/30/2019.
 */
public class Car {

  @Autowired
  private int year;
  String name;
  @Autowired
  List<Integer> cheirs;
  Map<String, String> system;
  Set<String> other;

  public int getYear() {
    return year;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Integer> getCheirs() {
    return cheirs;
  }

  public void setCheirs(List<Integer> cheirs) {
    this.cheirs = cheirs;
  }

  public Map<String, String> getSystem() {
    return system;
  }

  public void setSystem(Map<String, String> system) {
    this.system = system;
  }

  public Set<String> getOther() {
    return other;
  }

  public void setOther(Set<String> other) {
    this.other = other;
  }
}
