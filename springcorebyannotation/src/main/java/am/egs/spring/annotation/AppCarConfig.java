package am.egs.spring.annotation;

import am.egs.spring.annotation.others.Car;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by haykh on 4/30/2019.
 */
@Configuration
@ComponentScan(basePackages="am.egs.spring.annotation.Car")
public class AppCarConfig {
  @Bean
  public Car getCar() {
    return new Car();
  }

  @Bean
  public List<Integer> cheirs() {
    return Arrays.asList(100, 200);
  }

  @Bean
  public Integer year(){
    return 2010;
  }

  @Bean
  public Map<String, String> system(){
    Map<String, String>  map = new HashMap<String, String>();
    map.put("vin", "1234 5678 9123 1254");
    map.put("ml", "6543");
    return map;
  }
}
