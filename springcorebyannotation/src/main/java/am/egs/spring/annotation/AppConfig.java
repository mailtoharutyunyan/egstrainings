package am.egs.spring.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by haykh on 4/30/2019.
 */
@Configuration
@ComponentScan(basePackages="am.egs.spring.annotation.*")
public class AppConfig {

}
