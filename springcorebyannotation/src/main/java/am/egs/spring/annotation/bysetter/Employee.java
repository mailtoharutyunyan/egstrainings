package am.egs.spring.annotation.bysetter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by haykh on 4/29/2019.
 */
@Component
public class Employee {
  @Autowired
  private Department department;
}
