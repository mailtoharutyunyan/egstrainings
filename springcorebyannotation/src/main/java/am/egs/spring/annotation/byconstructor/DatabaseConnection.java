package am.egs.spring.annotation.byconstructor;

import org.springframework.stereotype.Component;

/**
 * Created by haykh on 4/29/2019.
 */
@Component
public class DatabaseConnection {
  private String url = "localhost";

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
