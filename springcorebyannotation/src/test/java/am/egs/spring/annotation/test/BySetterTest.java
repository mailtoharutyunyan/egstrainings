package am.egs.spring.annotation.test;

import am.egs.spring.annotation.AppConfig;
import am.egs.spring.annotation.bysetter.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by haykh on 4/30/2019.
 */
public class BySetterTest {

  private AnnotationConfigApplicationContext ctx;
  private Employee employee;

  @Before
  public void prepareTest() {
    ctx = new AnnotationConfigApplicationContext(AppConfig.class);
    employee = ctx.getBean(Employee.class);
  }

  @Test
  public void storeTest() {
    Assert.assertNotNull(employee);
  }

}
