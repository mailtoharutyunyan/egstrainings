package am.egs.spring.annotation.others.test;

import am.egs.spring.annotation.AppCarConfig;
import am.egs.spring.annotation.others.Car;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by haykh on 4/30/2019.
 */
public class OtherTest {

  @Test
  public void storeTest() {
    ApplicationContext ctx = new AnnotationConfigApplicationContext(AppCarConfig.class);
    Car car = ctx.getBean(Car.class);
    Assert.assertNotNull(car);
    Assert.assertEquals(2010, car.getYear());
  }

}
