package am.egs.spring.annotation.byconstructor.test;

import am.egs.spring.annotation.AppConfig;
import am.egs.spring.annotation.byconstructor.StoreService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by haykh on 4/30/2019.
 */
public class ByConstructorTest {
  private AnnotationConfigApplicationContext ctx;
  private StoreService storeService;

  @Before
  public void prepareTest() {
    ctx = new AnnotationConfigApplicationContext(AppConfig.class);
    storeService = ctx.getBean(StoreService.class);
  }

  @Test
  public void storeTest() {
    Assert.assertNotNull(storeService);
  }

  @Test
  public void storeDbConnectionTest() {
    Assert.assertEquals("localhost", storeService.showDBUrl());
  }

}
