package am.egs.spring;

/**
 * Created by haykh on 4/29/2019.
 */
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HelloWorld {
  public static final Log LOG = LogFactory.getLog(HelloWorld.class);

  public void sayHello() {
    LOG.info("Hello World!");
  }
}
