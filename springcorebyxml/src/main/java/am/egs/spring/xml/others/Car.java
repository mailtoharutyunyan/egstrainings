package am.egs.spring.xml.others;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by haykh on 4/30/2019.
 */
public class Car {
  int year;
  String name;
  List<Integer> cheirs;
  Map<String, String> system;
  Set<String> other;

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Integer> getCheirs() {
    return cheirs;
  }

  public void setCheirs(List<Integer> cheirs) {
    this.cheirs = cheirs;
  }

  public Map<String, String> getSystem() {
    return system;
  }

  public void setSystem(Map<String, String> system) {
    this.system = system;
  }

  public Set<String> getOther() {
    return other;
  }

  public void setOther(Set<String> other) {
    this.other = other;
  }
}
