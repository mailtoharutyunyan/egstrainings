package am.egs.spring.xml.byautowired;

/**
 * Created by haykh on 4/30/2019.
 */
public class University2 {
  private Student student;
  private Worker worker;

  public University2(Student student, Worker worker) {
    this.student = student;
    this.worker = worker;
  }

  public Student getStudent() {
    return student;
  }

  public Worker getWorker() {
    return worker;
  }
}
