package am.egs.spring.xml.byautowired;

/**
 * Created by haykh on 4/30/2019.
 */
public class University1 {

  private Student student;
  private Worker worker; //for testing purpose not Autowired it means no have worker definition in bean.xml
  private Worker otherWorker; //it will be Autowired only byType case byName case not autowired because no have otherWorker bean definition in bean.xml

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Worker getWorker() {
    return worker;
  }

  public void setWorker(Worker worker) {
    this.worker = worker;
  }

  public Worker getOtherWorker() {
    return otherWorker;
  }

  public void setOtherWorker(Worker otherWorker) {
    this.otherWorker = otherWorker;
  }
}
