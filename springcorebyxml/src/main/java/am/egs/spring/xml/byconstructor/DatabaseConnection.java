package am.egs.spring.xml.byconstructor;

public class DatabaseConnection {
  private String url = "localhost";

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
