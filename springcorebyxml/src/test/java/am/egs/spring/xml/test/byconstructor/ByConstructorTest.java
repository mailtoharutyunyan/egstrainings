package am.egs.spring.xml.test.byconstructor;

import am.egs.spring.xml.byconstructor.StoreService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class ByConstructorTest {
  ApplicationContext ctx;
  @Before
  public void initTest() {

    ctx = new ClassPathXmlApplicationContext("./beans_byconstructor.xml");
  }
  @Test
  public void storeTest() {
    StoreService storeService = ctx.getBean(StoreService.class);
    Assert.assertNotNull(storeService);
  }
  @Test
  public void storeDbConnectionTest() {
    StoreService storeService = ctx.getBean(StoreService.class);
    Assert.assertEquals("localhost", storeService.showDBUrl());
  }

}
