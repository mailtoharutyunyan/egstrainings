package am.egs.spring.xml.test.others;

import am.egs.spring.xml.others.Car;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by haykh on 4/30/2019.
 */
public class OtherTest {

  @Test
  public void storeTest() {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("./beans_others.xml");
    Car car = ctx.getBean(Car.class);
    Assert.assertNotNull(car);
    Assert.assertEquals(2010, car.getYear());
  }

}
