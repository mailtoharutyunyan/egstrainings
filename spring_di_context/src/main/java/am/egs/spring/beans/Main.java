package am.egs.spring.beans;

/**
 * Created by haykh on 4/29/2019.
 */
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main
{
  public static void main( String[] args )
  {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");

    Employee employee = ctx.getBean(Employee.class);

    Department department = ctx.getBean(Department.class);

    Operations operations = ctx.getBean(Operations.class);

    System.out.println(department);
    System.out.println(employee);

    operations.helloWorld();
  }
}
