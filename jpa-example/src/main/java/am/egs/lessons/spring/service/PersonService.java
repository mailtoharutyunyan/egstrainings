package am.egs.lessons.spring.service;

import java.util.List;

import am.egs.lessons.spring.entity.Person;

public interface PersonService {
    void add(Person person);
    List<Person> listPersons();
}
