package am.egs.lessons.spring.dao;

import java.util.List;

import am.egs.lessons.spring.entity.Person;

public interface PersonDao {
   void add(Person person);
   List<Person> listPersons();
}
